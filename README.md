### About
ReactJS-Typescript webpage with various tools to help with mobile game Dragon Lords. I'm a big fan of the game, and so this is a fan-created page. The official website for the game is at [https://www.dragonlordsmobile.com/](https://www.dragonlordsmobile.com/)

### Pages

#### Pact List Generator
- Copy paste the alliance kingdom list into the textbox and a pact list will be generated based on settings provided. There are two settings: prioritize bigs (good for farming) and prioritize evenness (best for war)

#### Troop Strength Calculator
- Compare the strength of your various troops with your opponents' army. Also useful as a way to look up stats of different troops

#### Help Section
- Provides tips about the game, along with some small tools to help out
