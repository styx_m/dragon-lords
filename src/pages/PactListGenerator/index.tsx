import * as React from 'react';
import CopyToClipboard from 'react-copy-to-clipboard';
import Alliance from './classes/Alliance';
import Pacts from './classes/Pacts';
import './index.css';

enum PactSetting {
  EVEN,
  FAVOR_BIGS
};

interface State {
  rawData: string;
  alliance: Alliance;
  copiedPlain: boolean;
  copiedMarkdown: boolean;
  PactSetting: PactSetting;
}

export default class PactListGenerator extends React.Component<{}, State> {

  constructor(props: {}) {

    super(props);

    this.state = {
      rawData: '',
      alliance: new Alliance(),
      copiedPlain: false,
      copiedMarkdown: false,
      PactSetting: PactSetting.EVEN
    };

    this.gatherData = this.gatherData.bind(this);
    this.generatePacts = this.generatePacts.bind(this);
    this.parseRawData = this.parseRawData.bind(this);
    this.updatePactSetting = this.updatePactSetting.bind(this);
  }

  updatePactSetting(e: React.ChangeEvent<HTMLSelectElement>) {

    this.setState({ PactSetting: +e.target.value });
    this.generatePacts(this.state.rawData, +e.target.value);
  }

  parseRawData(e: any) {
    this.generatePacts(e.target.value);
  }

  /**
   * Gathers data from the form and returns as array of objects {name, land} sorted by land descending
   */
  gatherData(rawData: string) {

    // in the form of Array(3) [[ "Daw777\n898 acres", "Daw777", "898" ]]
    const kingdomMatches =
      Array.from(rawData.matchAll(/(.+?)(?:\n|\s)?([\d,]+) acres/gi));

    const alliance = new Alliance();

    kingdomMatches.forEach(kingdom =>
      alliance.addKingdom(kingdom[1], parseInt(kingdom[2].replace(',', ''), 10)));

      return alliance;
  }

  generatePacts(rawData: string, pactSetting: PactSetting = this.state.PactSetting) {

    const alliance = this.gatherData(rawData);

    switch (pactSetting) {
      case PactSetting.FAVOR_BIGS:
        Pacts.generateBigFavoringPactList(alliance.kingdoms);
        break;
      default:
        Pacts.generateEvenPactList(alliance.kingdoms);
    }

    this.updatePacts(alliance, rawData);
  }

  updatePacts(alliance: Alliance, rawData: string) {

    // dont really even need this (well, except for rawData so the textArea updates), but just in case
    this.setState({
      alliance: alliance,
      rawData: rawData,
      copiedPlain: false,
      copiedMarkdown: false
    });
  }

  render() {

    const tableRows = this.state.alliance.kingdoms.map((kingdom) =>
      <tr key={kingdom.name}><td>{kingdom.name}</td><td>{kingdom.size.toLocaleString()}</td></tr>
    );

    const pactList = this.state.alliance.kingdoms.map(kingdom => {

      return <li key={kingdom.name}><strong>{kingdom.name}</strong> {` : ${kingdom.getPrettyPactPartners()}`}</li>
    });

    return (
      <div className="page">
        <h1>Pact List Generator</h1>
        <form className="pact-form">
            <label>Copy/paste alliance kingdom list here (can just select all on alliance members list). Note: If any members have numbers in their username, you must go to their name and make a space or a new line between their name and their land
              <br />
              <textarea className="raw-data" value={this.state.rawData} onChange={this.parseRawData}></textarea>
            </label>
            <br />
            <label>Prioritize pacts based on:&nbsp;
              <select value={this.state.PactSetting} onChange={this.updatePactSetting}>
                <option value={PactSetting.EVEN}>Evenness</option>
                <option value={PactSetting.FAVOR_BIGS}>Bigs</option>
              </select>
            </label>
            <button type="button">Generate List</button>
        </form>
        <table>
          <caption className="kingdoms-found">Kingdoms Found</caption>
            <tbody>
              {tableRows.length < 1 ? <tr><td>No Kingdoms Found</td></tr> : tableRows}
              </tbody>
        </table>

        <h3>Pact List</h3>
          <ul>{pactList.length < 1 ? <p>No Kingdoms Found</p> : pactList}</ul>

        <form>
          <CopyToClipboard text={Pacts.getPrettyStr(this.state.alliance.kingdoms)} onCopy={() => this.setState({copiedPlain: true})}>
            <button type="button">Copy plain text</button>
          </CopyToClipboard>
          <span className={this.state.copiedPlain ? 'success' : 'hidden'}> Copied</span>
            <CopyToClipboard text={Pacts.getPrettyStr(this.state.alliance.kingdoms, true)} onCopy={() => this.setState({copiedMarkdown: true})}>
            <button type="button">Copy for use in 3rd party apps</button>
          </CopyToClipboard>
          <span className={this.state.copiedMarkdown ? 'success' : 'hidden'}> Copied</span>
        </form>

        <details>
            <summary>Help</summary>
              <p>Copy/paste kingdoms on the alliance members tab. You can just go to the tab and select all text on it and as long as the members and their land are included you're good to go. My program will parse the text for the relevant information. Pacts are created by adding the first 2 members on either side, sizewise, then gives highest land kingdoms a pact with the next highest that they don't already have. If in an uneven alliance, the smallest kingdom gets 4 pacts</p>
        </details>
        <details>
          <summary>Nerds Only</summary>
            <p>Copy for use in 3rd party apps uses markdown syntax</p>
            <p>The regex matching is <code>/(.+?)(?:\n|\s)?([\d,]+) acres/gi</code></p>
        </details>
      </div>
    )
  }
}

