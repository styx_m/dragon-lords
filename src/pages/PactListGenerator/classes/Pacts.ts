import Alliance from "./Alliance";
import Kingdom from "./Kingdom";

export default class Pacts {

  static maxAllowed = 5;

  static generateEvenPactList(kingdoms: Kingdom[]) {

    kingdoms.forEach((kingdom: Kingdom, i: number) => {

      // first 4 pacts for everyone are the two kingdoms adjacent in size, wrapping around
      const adjacentKingdomIndices = [
        (i+1) % kingdoms.length,
        (i+2) % kingdoms.length,
        (i - 1) < 0 ? kingdoms.length - i - 1 : i-1,
        i - 2 < 0 ? kingdoms.length-i-2 : i-2
      ]

      for (let j = 0; j < Pacts.maxAllowed - 1; j++) {
        kingdoms[i].addPact(kingdoms[adjacentKingdomIndices[j]]);
      }
    });

    // after each kingdom has 4 pacts, start from highest land and give them the highest land pact they dont already have
    // if uneven number, least land gets 1 fewer pact
    kingdoms.forEach((kingdom, i) => {

      let kingdomIdx = 0;
      while (kingdomIdx < kingdoms.length && !kingdom.hasMaxPacts()) {
        kingdom.addPact(kingdoms[kingdomIdx]);
        kingdomIdx++;
      }
    });

    return kingdoms.sort(Alliance.sortByLand)
  }

  static generateBigFavoringPactList(kingdoms: Kingdom[]) {

    kingdoms.forEach(kingdom => {
      for (let j = 0; j < Pacts.maxAllowed; j++) {
        kingdom.addPact(kingdoms[j]);
      }
    })

    let currentKingdomIdx = kingdoms.length - 1;

    // starting from bottom of kingdom list, see if can get that kingdom a pact (if not full) by checking
    // if can break up a pact with other lowest level kingdoms
    while (currentKingdomIdx > -1) {

      let lowestLandKingdomIdx = kingdoms.length - 1;

      while (!kingdoms[currentKingdomIdx].hasMaxPacts() && lowestLandKingdomIdx > -1) {

        if (!kingdoms[lowestLandKingdomIdx].hasMaxPacts()) {
          kingdoms[currentKingdomIdx].addPact(kingdoms[lowestLandKingdomIdx]);
          lowestLandKingdomIdx--;
          continue;
        }

        for (let i = kingdoms[lowestLandKingdomIdx].pacts.length - 1; i > -1; i--) {

          let merged = kingdoms[currentKingdomIdx].mergePactsWith(kingdoms[lowestLandKingdomIdx], kingdoms[lowestLandKingdomIdx].pacts[i]);

          if (merged) {
            break;
          }
        }
        lowestLandKingdomIdx--;
      }
      currentKingdomIdx--;
    }

    return kingdoms.sort(Alliance.sortByLand)
  }

  static getPrettyStr(kingdoms: Kingdom[], markdown = false) {

      let str = '';
      kingdoms.forEach(kingdom => {
        str += `${kingdom.name}: ${kingdom.getPrettyPactPartners()}\n`;
      });

      if (markdown) {
        return str.replace(/(.+):/ig, '**$1** :');
      }

      return str;
  }
}
