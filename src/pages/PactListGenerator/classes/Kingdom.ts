import Alliance from "./Alliance";
import Pacts from "./Pacts";

export default class Kingdom {

  name = '';
  size = 0;
  pacts: Array<Kingdom> = [];

  constructor(name: string, size: number) {
    this.name = name;
    this.size = size;
  }

  addPact(kingdom: Kingdom) {

    // if (this && kingdom) console.log('testing', this.name, kingdom.name);
    if (this.canCreatePactWith(kingdom)) {
      this.pacts.push(kingdom);
      kingdom.pacts.push(this);

      console.log('creating pact ', this.name, kingdom.name)
      return true;
    }
    return false;
  }

  canReplacePactWith(toBeReplaced: Kingdom, replacer: Kingdom) {
    return !!toBeReplaced && !!replacer && replacer.hasPactWith(toBeReplaced) && !this.hasPactWith(replacer) && !this.hasPactWith(toBeReplaced);
  }

  mergePactsWith(kingdom1: Kingdom, kingdom2: Kingdom) {

    if (!this.canReplacePactWith(kingdom1, kingdom2)) {
      return false;
    }

    let canRemovePacts = kingdom1.removePactWith(kingdom2);

    if (!canRemovePacts) {
      console.log('Cannot remove pacts');
      return false;
    }

    return this.addPact(kingdom1) && this.addPact(kingdom2);
  }

  replacePactsWith(toBeReplaced: Kingdom, replacer: Kingdom) {

    if (!this.canReplacePactWith(toBeReplaced, replacer)) {
      return false;
    }

    this.removePactWith(toBeReplaced);
    this.addPact(replacer);
    return true;
  }

  removePactWith(kingdom: Kingdom) {

    if (!this.hasPactWith(kingdom)) {
      console.log('Not removed: ', this.name, 'does not have pact with', kingdom.name);
    }

    let kingdomIdx = this.pacts.indexOf(kingdom);
    let removed = this.pacts.splice(kingdomIdx, 1);

    if (!removed) {
      console.log(this.name, 'did not remove', kingdom.name);
      return false;
    }

    let otherKingdomIdx = kingdom.pacts.indexOf(this);
    let removedFromOtherKingdom = kingdom.pacts.splice(otherKingdomIdx, 1);

    if (!removedFromOtherKingdom) {
      console.log(this.name, 'did not remove', kingdom.name);
      return false;
    }

    return true;
  }

  canCreatePactWith(kingdom: Kingdom) {
    return !!kingdom &&
      kingdom !== this &&
      !this.hasMaxPacts() &&
      !kingdom.hasMaxPacts() &&
      !this.hasPactWith(kingdom);
  }

  hasPactWith(kingdom: Kingdom) {
    const hasPact = this.pacts.includes(kingdom);
    if (hasPact) console.log('hasPact');
    return hasPact;
  }

  hasMaxPacts() {
    const maxPacts = this.pacts.length >= Pacts.maxAllowed;
    if (maxPacts) console.log('max');
    return maxPacts;
  }

  getPrettyPactPartners() {

      this.pacts.sort(Alliance.sortByLand)

      let partners = this.pacts.reduce((str, cur) => {
        str += `${cur.name}, `
        return str;
      }, '');

      partners = partners.substr(0, partners.length - 2);

      return partners;
  }
}


