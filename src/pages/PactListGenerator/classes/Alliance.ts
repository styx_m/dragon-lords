import Kingdom from './Kingdom';

export default class Alliance {

  kingdoms: Kingdom[] = [];

  addKingdom(name: string, size: number) {

      this.kingdoms.push(new Kingdom(name, size));
      this.kingdoms.sort(Alliance.sortByLand);
  }

  static sortByLand(a: Kingdom, b: Kingdom) {
    return b.size - a.size;
  }
}
