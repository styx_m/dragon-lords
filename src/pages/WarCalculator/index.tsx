import * as React from 'react';
import Army from './classes/Army';
import IndividualArmyCalculator from './components/AddTroop/container'
import './index.css';

interface State {
  armies: Army[];
}

export default class WarCalculator extends React.Component<{}, State> {

  constructor(props: any) {

    super(props);

    this.state = {armies: [new Army(), new Army()]}
  }

  onUpdate(i: number, army: Army) {

    // doing copies a lot because dont like mutations
    const newArmies = [this.state.armies[0].copy(), this.state.armies[1].copy()];
    newArmies[i] = army.copy();
    this.setState({ armies: newArmies} );
  }

  render() {

    const attackWin = this.state.armies[0].calculateAttack() > this.state.armies[1].calculateDefense();
    const defenseWin = this.state.armies[0].calculateDefense() > this.state.armies[1].calculateAttack();

      return (
      <div className="page">
      <h1>War Calculator</h1>
      <div className="container">
        <IndividualArmyCalculator
          army={this.state.armies[0]}
          onUpdate={(army: Army) => this.onUpdate(0, army)}
        />
        <section>
          <span className={attackWin ? 'success' : 'failure'}>Attack</span> | <span className={defenseWin ? 'success' : 'failure'}>Defense</span>
        </section>
        <IndividualArmyCalculator
          army={this.state.armies[1]}
          onUpdate={(army: Army) => this.onUpdate(1, army)}
        />
        </div>
      </div>
      );
  }
}
