import Troop from '../interfaces/Troop';

export default [
  {
    "name": "select troop",
    "attack": 0,
    "defense": 0,
    "enlisted": null
  },
  {
    "name": "militia",
    "attack": 0,
    "defense": 1,
    "enlisted": 0
  },
  {
    "name": "tier1",
    "attack": 5,
    "defense": 5,
    "enlisted": 0
  },
  {
    "name": "tier2",
    "attack": 15,
    "defense": 15,
    "enlisted": 0
  },
  {
    "name": "tier3",
    "attack": 30,
    "defense": 30,
    "enlisted": 0
  },
  {
    "name": "tier4",
    "attack": 30,
    "defense": 75,
    "enlisted": 0
  },
  {
    "name": "tier5",
    "attack": 75,
    "defense": 30,
    "enlisted": 0
  },
  {
    "name": "battering ram",
    "attack": 400,
    "defense": 0,
    "enlisted": 0
  },
  {
    "name": "balista",
    "attack": 500,
    "defense": 300,
    "enlisted": 0
  },
  {
    "name": "boiling tar",
    "attack": 0,
    "defense": 1000,
    "enlisted": 0
  },
  {
    "name": "seige tower",
    "attack": 1400,
    "defense": 0,
    "enlisted": 0
  },
  {
    "name": "elemental warden",
    "attack": 0,
    "defense": 75,
    "enlisted": 0
  },
  {
    "name": "flame legion",
    "attack": 50,
    "defense": 40,
    "enlisted": 0
  },
  {
    "name": "demon",
    "attack": 250,
    "defense": 250,
    "enlisted": 0
  },
  {
    "name": "reptilian",
    "attack": 2000,
    "defense": 2000,
    "enlisted": 0
  },
  {
    "name": "skoffin",
    "attack": 2200,
    "defense": 2200,
    "enlisted": 0
  },
  {
    "name": "unholy",
    "attack": 2400,
    "defense": 2400,
    "enlisted": 0
  },
  {
    "name": "wyrm",
    "attack": 25000,
    "defense": 25000,
    "enlisted": 0
  },
  {
    "name": "collosus",
    "attack": 0,
    "defense": 0,
    "enlisted": 0
  },
  {
    "name": "phoenix",
    "attack": 5000,
    "defense": 5000,
    "enlisted": 0
  },
  {
    "name": "dragon",
    "attack": 0,
    "defense": 0,
    "enlisted": 0
  },
  {
    "name": "seraph",
    "attack": 0,
    "defense": 0,
    "enlisted": 0
  },
  {
    "name": "archangel",
    "attack": 0,
    "defense": 0,
    "enlisted": 0
  },
  {
    "name": "hill giant",
    "attack": 2500,
    "defense": 2500,
    "enlisted": 0
  },
] as Troop[]
