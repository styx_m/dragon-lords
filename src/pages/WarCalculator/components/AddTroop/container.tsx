import * as React from 'react';
import Unit from '../../classes/Unit';
import Army from '../../classes/Army';
import AddTroop from './index'

interface Props {
  army: Army;
  onUpdate: Function;
}

interface State {
  army: Army;
}

export default class AddTroopContainer extends React.Component<Props, State> {

  constructor(props: any) {

    super(props);

    this.state = {army: this.props.army}

    this.onAdd = this.onAdd.bind(this);
    this.onRemove = this.onRemove.bind(this);
  }

  onAdd(unit: Unit) {

    if (unit.enlisted === 0) return;

    console.log("onAdd", unit);

    const currState = this.state.army.copy();
    // const curTroopInfo = currState.get(troop.name);
    currState.updateUnit(unit);

    this.setState({ army: currState });
    this.props.onUpdate(currState);
  }

  onRemove(unit: Unit) {

      const currState = this.state.army.copy();
      currState.deleteUnit(unit);

      this.setState({ army: currState });
      this.props.onUpdate(currState);
  }

  render() {

    const troopsElt = [] as JSX.Element[];

    troopsElt.push(<AddTroop key="-1" initialInput={true} unit={new Unit()} onAdd={this.onAdd} onRemove={this.onRemove} />);

    console.log(this.state.army.getAsArray());
    this.state.army.getAsArray().forEach((unit) => {
      console.log(unit.calculateAttack);
      troopsElt.push(
        <AddTroop unit={unit} key={unit.name+unit.enlisted} onAdd={this.onAdd} onRemove={this.onRemove} />
      );
    });

    return (
      <section>
        {troopsElt}
        <output>
          <output>Total Attack: {`${this.state.army.calculateAttack().toLocaleString()}`},&nbsp;</output>
          <output>Total Defense: {`${this.state.army.calculateDefense().toLocaleString()}`}</output>
        </output>
        <details>
          <summary>Help</summary>
            <p>Select a unit, then choose how many of that unit are in the army. Individual attack/defense per unit is listed below the "type" dropdown, while the total strength of that unit is listed below "amount."</p>
            <p>It's possible to update a unit after adding it in two ways: either by going to its box and updating the value directly, or by using the first box to select the troop again and change the amount or delete the unit.</p>
              <p>In the interest of efficiency, there's also two ways to add troops. If you hit the "submit" button on the first box, the box will auto clear and the troops will be added. If you click anywhere outside the box without hitting the submit button, the information will still be updated, but the initial box will not be cleared.</p>
        </details>
      </section>
    );
  }
}

