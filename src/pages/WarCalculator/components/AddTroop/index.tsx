import * as React from 'react';
import FormContainer from '../../../../shared-components/Form';
import troops from '../../data/troops';
import Unit from '../../classes/Unit';
import Troop from '../../interfaces/Troop';
import './index.css';

interface Props {
  onAdd: Function;
  onRemove: Function;
  unit: Unit;
  initialInput?: boolean;
}

interface State {
  unit: Unit;
}

export default class AddTroop extends React.Component<Props, State> {

  constructor(props: Props) {

    super(props);

    this.state = {
      unit: this.props.unit
      // amount: this.props.troop ? this.props.troop.enlisted : 0
    }

    this.updateAmount = this.updateAmount.bind(this);
    this.updateType = this.updateType.bind(this);
    this.onAdd = this.onAdd.bind(this);
    this.onRemove = this.onRemove.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  updateType(e: React.ChangeEvent<HTMLSelectElement>) {

    const unit = new Unit(JSON.parse(e.target.value));
    unit.enlisted = this.state.unit.enlisted;
    this.setState({ unit });
  }

  updateAmount(e: React.ChangeEvent<HTMLInputElement>) {

    const unit = new Unit(this.state.unit);
    unit.enlisted = +e.target.value;
    // this.props.onAdd(unit);
    this.setState({ unit });
  }

  onAdd() {
      this.props.onAdd(this.state.unit);

  }

  onSubmit(e: React.FormEvent<HTMLFormElement>) {
      e.preventDefault();
      this.onAdd();
      if (this.props.initialInput) this.reset();
  }

  onRemove() {
      this.props.onRemove(this.state.unit);
      this.reset();
  }

  reset() {
      this.setState({ unit: new Unit() }); // reset inputs
  }

  setOptionValue(troop: Troop) {
      return troop.name === this.state.unit.name ? this.state.unit : troop;
  }

  render() {

    const troopOptions = troops.map(troop =>
      <option key={troop.name} value={JSON.stringify(this.setOptionValue(troop))}>
        {troop.name}
        </option>
     );

    return (
      <FormContainer
        className={`addTroop ${this.props.initialInput ? 'initialInput' : ''}`}
        onSubmit={this.onSubmit}
        onBlur={this.onAdd}
      >

          <div>
            <label>Type:
                <select value={JSON.stringify(this.state.unit)} onChange={this.updateType}>
                  { troopOptions }
                </select>
            </label>
            <output>
              <span className="individualStats">
                Attack: {`${this.state.unit.attack.toLocaleString()}`}&nbsp;
              </span>
                <span className="individualStats">
                  Defense: {`${this.state.unit.defense.toLocaleString()}`}
                </span>
            </output>
            </div>

        <div>
          <label>Amount:
            <input type="number" name="amount" value={this.state.unit.enlisted || ''} onChange={this.updateAmount} />
          </label>
          <output>
            <span className="individualStats">
              Attack: {`${this.state.unit.calculateAttack().toLocaleString()}`},&nbsp;
            </span>
            <span className="individualStats">
              Defense: {`${this.state.unit.calculateDefense().toLocaleString()}`}
            </span>
          </output>
        </div>

        <input type="submit" value="Update" />
        <input type="button" onClick={this.onRemove} value="Delete" />

      </FormContainer>
    )
  }
}
