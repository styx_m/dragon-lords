export default interface Troop {
  name: string,
  attack: number,
  defense: number,
  enlisted: number
}
