import Unit from './Unit';

export default class Army {

    units = new Map<string, Unit>();

    get(name: string) {
        return this.units.get(name);
    }

    copy() {
        const newArmy = new Army();
        this.getAsArray().forEach((unit) => newArmy.updateUnit(unit.copy()));
        return newArmy;
    }

    getAsArray() {
        return Array.from(this.units.values());
    }

    updateUnit(unit: Unit) {
        this.units.set(unit.name, unit.copy());
    }

    deleteUnit(unit: Unit) {
        this.units.delete(unit.name);
    }

    calculateAttack() {
        console.log('here');
        return this.getAsArray()
        .reduce((accum, curUnit) => accum + curUnit.calculateAttack(), 0);
    }

    calculateDefense() {
        return this.getAsArray()
        .reduce((accum, curUnit) => accum + curUnit.calculateDefense(), 0);
    }
}
