import Troop from '../interfaces/Troop';

export default class Unit {

    name = "";
    attack = 0;
    defense = 0;
    enlisted = 0;

    constructor(troop?: Troop) {

        if (!troop) return this;
        this.name = troop.name;
        this.attack = troop.attack;
        this.defense = troop.defense;
        this.enlisted = troop.enlisted;
    }

    calculateAttack() {
        return this.attack * this.enlisted;
    }

    calculateDefense() {
        return this.defense * this.enlisted;
    }

    copy() {
        return new Unit(this); // no reference dt here, so effectively copies
    }
}
