import * as React from 'react';
import { Link } from 'react-router-dom';

export default function Home() {
  return (
    <div className="page">
      <h1>Styx's Dragon Lords Tools</h1>
      <ul>
        <li><a href="https://www.dragonlordsmobile.com/">Official DL Site</a></li>
        <li><Link to="/help/">Help</Link></li>
        <li><Link to="/tools/troop-strength-calculator">Troop Strength Calculator</Link></li>
        <li><Link to="/tools/pact-list-generator">Pact List Generator</Link></li>
      </ul>
      <p>If you find a bug in anything or would like to suggest additional features/programs, pm Styx</p>

      Known Bugs:
      <ul>
          <li>War calculator - Colluses/archangel/seraph have stats of 0. This bug is due to me not knowing their stats</li>
      </ul>

      <p>This site was created using Typescript React, and is open-source. PM me for the code.</p>
      <h5>0.0</h5>
    </div>
  );
}
