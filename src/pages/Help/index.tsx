import * as React from 'react';
import { Link } from 'react-router-dom';
import EngineCalculator from './EngineCalculator';

export default function Help() {
  return (
    <div className="page">
      <h1>Help</h1>
      <h3>Reddit Posts</h3>

        <ul>
          <li>
            <a href="https://www.reddit.com/r/DragonLordsReborn/comments/gwh0nr/exploration_bonuses_may_2020/">Age/Protection Based Exploration Buffs</a>
          </li>
          <li>
            <a href="https://www.reddit.com/r/DragonLordsReborn/comments/erxhln/new_boss_bonuses/">Boss Drops</a>
          </li>
        </ul>

      <h3>Answers To Commonly Asked Questions</h3>
        <h4>Exploration</h4>
        <ul>
          <li>Relics stack at 100% efficiency.</li>
          <li>Spells and scrolls stack at 100%, 80%, 60%, 40%, 20%, and 10% thereafter.</li>
          <li>Explore Stormland (zone 4) for mighty relics and IX scrolls "relatively" frequently. Plateau of Fire (zone 4) drops X scrolls and draks (albeit less frequently).</li>
        </ul>
        <h4>War</h4>
          <ul>
            <li>You can have half as many seige engines as land, and each engine requires 10 builders to maintain. For example: <EngineCalculator /></li>
            <li>If you would like to know the strength of various troops, visit the <Link to="/tools/troop-strength-calculator">war calculator</Link>.</li>
        </ul>
    </div>
  );
}

