import * as React from 'react';

interface State {
  land: number;
  engines: number;
  builders: number;
}

export default class EngineCalculator extends React.Component<{}, State> {

  constructor(props: {}) {

    super(props);

    this.state = this.calculateEngines(1000);

    this.updateEngines = this.updateEngines.bind(this);
  }

  calculateEngines(land: number) {

    const engines = Math.floor(land / 2);
    const builders = engines * 10;

    return { land, engines, builders };

  }

  updateEngines(e: React.ChangeEvent<HTMLInputElement>) {
    this.setState(this.calculateEngines(+e.target.value));
  }

  render() {

    return (
      <span>
        If you have <input type="number" name="land" value={this.state.land || ''} onChange={this.updateEngines} /> land you can have <em>{this.state.engines.toLocaleString()}</em> engines, which requires <em>{this.state.builders.toLocaleString()}</em> builders.
      </span>

    )
  }
}


