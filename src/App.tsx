import * as React from 'react';
import Home from './pages/Home'
import PactListGenerator from './pages/PactListGenerator';
import AttackCalculator from './pages/WarCalculator'
import Help from './pages/Help';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import './App.css'

function App() {
  return (
    <div>
      <Router basename="/dragon-lords">
        <div>
          <nav className="nav-bar">
            <ul>
              <li> <Link to="/">Home</Link> </li>
              <li> <Link to="/help">Help</Link> </li>
              <li> <Link to="/tools/pact-list-generator">Pact List Generator</Link> </li>
              <li> <Link to="/tools/troop-strength-calculator">War Calculator</Link> </li>
            </ul>
          </nav>
          <Switch>
            <Route path="/help">
              <Help />
            </Route>
            <Route path="/tools/pact-list-generator">
              <PactListGenerator />
            </Route>
            <Route path="/pact-list-generator">
              <PactListGenerator />
            </Route>
            <Route path="/tools/troop-strength-calculator">
              <AttackCalculator />
            </Route>
            <Route path="/war-calculator">
              <AttackCalculator />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </div>
    </Router>
  </div>
  );
}

export default App;
